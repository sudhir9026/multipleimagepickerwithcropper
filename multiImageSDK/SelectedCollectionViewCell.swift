//
//  SelectedCollectionViewCell.swift
//  multiImageSDK
//
//  Created by sudhir on 9/28/18.
//  Copyright © 2018 Sofmen. All rights reserved.
//

import UIKit

class SelectedCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var photoImage: UIImageView!
    @IBOutlet var videoPlayImage: UIImageView!
    
    override func awakeFromNib() {
        
        photoImage.layer.borderColor = UIColor.blue.cgColor
        photoImage.layer.borderWidth = 1
        photoImage.layer.masksToBounds = true
    }
    
}
