//
//  SFImagePickerManager.swift
//  multiImageSDK
//
//  Created by sudhir on 9/25/18.
//  Copyright © 2018 Sofmen. All rights reserved.
//

import Foundation
import Photos
import AVKit

struct PhotoCollection {
    
    var assets: PHAsset
    var created: Date?
    var createdDate: String?
}

enum SelectMediaType {
    
    case OnlyPhoto
    case OnlyVideo
    case Both
}

typealias successCompletion = (([PHAsset]) -> Void)?
typealias cancelCompletion = (() -> Void)?

protocol SFImagePickerControllerProtocol {
    
    func didFinishWithPickingAssets(assets: [PHAsset])
    func imagePickerDidCancel()
}

class SFImagePickerClosuresManager: NSObject, SFImagePickerControllerProtocol {
    
    var selectAssets: successCompletion
    var cancel: cancelCompletion
    
    static var sharedManager = SFImagePickerClosuresManager()
    override init() { }
    
    func didFinishWithPickingAssets(assets: [PHAsset]) {
        selectAssets!(assets)
    }
    func imagePickerDidCancel() {
        cancel!()
    }
}

class SFImagePickerManager {
    
    public var selectMediaType : SelectMediaType
    public var cameraPreview : Bool
    public var maximumSelectionCount : Int
    public var isAutoDismissOnMaximum : Bool


    
    init(mediaType : SelectMediaType = .OnlyPhoto, maximumSelectionCount : Int = -1 ,isAutoDismissOnMaximum : Bool = false, isCameraPreview: Bool = false ) {
        
        self.selectMediaType = mediaType
        self.cameraPreview = isCameraPreview
        self.maximumSelectionCount = maximumSelectionCount
        self.isAutoDismissOnMaximum = isAutoDismissOnMaximum
        
    }
    
    func presentSDImagePickerController(_ selfController: UIViewController, selectBlock:successCompletion , cancelBlock: cancelCompletion)  {
        
        let manager = SFImagePickerClosuresManager.sharedManager
        manager.selectAssets = selectBlock
        manager.cancel = cancelBlock
        
        let rootVC = SFRootPickerViewController(nibName: "SFRootPickerViewController", bundle: nil)
        rootVC.finishDelegate = manager
        
        rootVC.selectMediaType = selectMediaType
        rootVC.isCameraPreview = cameraPreview
        rootVC.maximumSelectionCount = maximumSelectionCount
        rootVC.isAutoDismissOnMaximum = isAutoDismissOnMaximum
    
        selfController.present(rootVC, animated: true, completion: nil)
        
    }
    
}

//  Videp play
extension SFImagePickerManager {
    
    func playVideo(_ selfController: UIViewController, asset: PHAsset) {
        
        requestAVAsset(asset: asset) { (avAsset) in
            
            guard let avVideoAsset = avAsset else { selfController.generateAlert(message: "unable to play this video.")
                return }
            
            DispatchQueue.main.async(execute: { () in
                
                let avPlayerItem = AVPlayerItem(asset: avVideoAsset)
                
                let avPlayer = AVPlayer(playerItem: avPlayerItem)
                let player = AVPlayerViewController()
                player.player = avPlayer
                
                avPlayer.play()
                
                selfController.present(player, animated: true, completion: nil)
                
            })
        }
        
    }
    
    private func requestAVAsset(asset: PHAsset, completeBlock: @escaping (_ AVAsset: AVAsset?) -> Void) {
        
        let semaphore = DispatchSemaphore.init(value: 1)
        let imageManager = PHImageManager()
        var avAsset: AVAsset?
        semaphore.wait()
        imageManager.requestAVAsset(forVideo: asset, options: nil) { (asset, _, _) in
            avAsset = asset
            completeBlock(avAsset)
            semaphore.signal()
        }
    }
}
