//
//  SFHelperClass.swift
//  multiImageSDK
//
//  Created by sudhir on 9/27/18.
//  Copyright © 2018 Sofmen. All rights reserved.
//

import UIKit


struct Constant {
    
//    NIB
    static let ImageCellNib = "SFImageCollectionViewCell"
    static let VideoCellNib = "SFVideoCollectionViewCell"
    static let HeaderCellNib = "SFHeaderCollectionReusableView"
    
//    Cell Indentifie
    
    static let ImageCellIndentifier = "ImageCell"
    static let VideoCellIndentifier = "videoCell"
    static let HeaderCellIndentifier = "headerReusableView"

    

    
//    ALERT MESSAGE
    static let BlankSelect = "Please select at least one."
    static let DisableLibrary = "Please give access permission of Photos.\n\n Go To Settings -> Your APP -> Photos."
    static let DisableCamera = "Please give access permission of Camera.\n\n Go To Settings -> Your APP -> Camera."

    static let OK = "OK"
    
    
//    KEY
    static let DateFormat = "MMMM d, yyyy"
    static let MediaType = "mediaType == %d"
    static let CreateDate = "creationDate"
    
//
    
    static let CameraJPG = "open_camera.jpg"
}

extension Date
{
    func toString( dateFormat format  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
}

extension UIViewController {
    
    func generateAlert(message: String) {
        
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: Constant.OK, style: .cancel, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
        
    }
    
}

