//
//  SFRootPickerViewController.swift
//  multiImageSDK
//
//  Created by sudhir on 9/25/18.
//  Copyright © 2018 Sofmen. All rights reserved.
//

import UIKit
import Photos

class SFRootPickerViewController: UIViewController {
    
    var selectMediaType : SelectMediaType!
    var isCameraPreview : Bool!
    var maximumSelectionCount : Int!
    var isAutoDismissOnMaximum : Bool!
    
    /// Private Properties
    private var sortedPhoto : [PhotoCollection] = []
    private var totalAssetItems : [String: Any] = [:]
    private var selectedAssetItems : [PHAsset]? = []
    private var numberOfSectionKeys : [String] = []
    private var itemAssetsList:  PHFetchResult<PHAsset>? = nil
    private var isCameraTakingPic: Bool = false
    private var numberOfCellInRow: CGFloat = 3.0

    
    
    lazy var activityIndicatorView: UIActivityIndicatorView = {
        
        let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        myActivityIndicator.hidesWhenStopped = true
        return myActivityIndicator
    }()
    
    var finishDelegate : SFImagePickerControllerProtocol?
    
//    Outlets properties
    @IBOutlet weak var photoCollectionView: UICollectionView!
    
    @IBOutlet var titleNavigateView: UINavigationItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.photoCollectionView.register(UINib(nibName: Constant.ImageCellNib, bundle: nil), forCellWithReuseIdentifier:SFImageCollectionViewCell.reuseId)
        self.photoCollectionView.register(UINib(nibName: Constant.VideoCellNib, bundle: nil), forCellWithReuseIdentifier: SFVideoCollectionViewCell.reuseId)
        self.photoCollectionView.register(UINib(nibName: Constant.HeaderCellNib, bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier:SFHeaderCollectionReusableView.reuseId)
        
        self.view.addSubview(activityIndicatorView)
        
        let pinch = UIPinchGestureRecognizer.init(target: self, action: #selector(handlePinchGesture))
        photoCollectionView.addGestureRecognizer(pinch)
        
        requestAuthorizationOfLibrary()
    }
    
    @objc func handlePinchGesture(sender:AnyObject) {

        let senderGR = sender as! UIPinchGestureRecognizer
        
        if senderGR.state == .ended {
            if senderGR.scale < 1 {
                
                if numberOfCellInRow < 6 {
                    numberOfCellInRow = numberOfCellInRow + 1
                    photoCollectionView.reloadData()
                    
                }
                
            } else {
                
                if numberOfCellInRow > 2 {
                    numberOfCellInRow = numberOfCellInRow - 1
                    photoCollectionView.reloadData()
                    
                }
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        
        finishDelegate?.imagePickerDidCancel()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneButtonTapped(_ sender: Any) {
        guard (selectedAssetItems?.count)! > 0  else { generateAlert(message: Constant.BlankSelect)
            return
        }

        finishDelegate?.didFinishWithPickingAssets(assets: selectedAssetItems!)
        self.dismiss(animated: true, completion: nil)
    }

}

// MARK: All private methods
extension SFRootPickerViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
   
    private func requestAuthorizationOfLibrary() {
        
        PHPhotoLibrary.requestAuthorization { (status) in
            
            switch status {
                
            case .authorized :
                self.fetchAssets()
                
            case .notDetermined:
                
                self.generateAlert(message: Constant.DisableLibrary)
                
            case .restricted, .denied:
                
                self.generateAlert(message: Constant.DisableLibrary)
            }
            
        }
    }
    
    private func fetchAssets() {
        
        let fetchOption = createDefaultAssetFetchOptions()
        itemAssetsList = PHAsset.fetchAssets(with: fetchOption)
        guard let imageList = itemAssetsList else { return }
        sortedPhoto.removeAll()
        for i in 0 ..< imageList.count {
            
            let assets = imageList.object(at: i)
            
            let temoCollection = PhotoCollection(assets: assets, created: assets.creationDate ,createdDate: assets.creationDate?.toString(dateFormat: Constant.DateFormat) )
            sortedPhoto.append(temoCollection)
        }
        
        filterData()
        
    }
    
    private func filterData()  {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constant.DateFormat
        totalAssetItems = Dictionary(grouping: sortedPhoto, by: { $0.createdDate! })
        
        numberOfSectionKeys = Array(totalAssetItems.keys.sorted(by: { dateFormatter.date(from:$0)?.compare(dateFormatter.date(from:$1)!) == .orderedDescending }))
        
        DispatchQueue.main.async {
            
            if self.isCameraTakingPic {
                
                self.selectFirstImageAfterTakingCameraPic()
            }
            self.photoCollectionView.reloadData()
        }
    }
    
    private func createDefaultAssetFetchOptions() -> PHFetchOptions {
        
        let createImagePredicate = { () -> NSPredicate in
            let imagePredicate = NSPredicate(format: Constant.MediaType, PHAssetMediaType.image.rawValue)
            
            return imagePredicate
        }
        
        let createVideoPredicate = { () -> NSPredicate in
            let videoPredicate = NSPredicate(format: Constant.MediaType, PHAssetMediaType.video.rawValue)
            
            return videoPredicate
        }
        
        var predicate: NSPredicate?
        
        switch self.selectMediaType {
            
        case .Both :
            predicate = NSCompoundPredicate(orPredicateWithSubpredicates: [createImagePredicate(), createVideoPredicate()])
        case .OnlyPhoto:
            predicate = createImagePredicate()
        case .OnlyVideo:
            predicate = createVideoPredicate()
            
        case .none: break
            
        case .some(_): break
            
        }
        
        let assetFetchOptions = PHFetchOptions()
        assetFetchOptions.sortDescriptors = [NSSortDescriptor(key: Constant.CreateDate, ascending: false)]
        assetFetchOptions.predicate = predicate
        
        return assetFetchOptions
    }
    
    private func selectFirstImageAfterTakingCameraPic() {
        
        let rowkey = self.numberOfSectionKeys[0] as String
        let rowArray = self.totalAssetItems[rowkey] as! [PhotoCollection]
        let rowDict = rowArray[0]
        let assets = rowDict.assets
        self.selectedAssetItems?.append(assets)
        actionAfterSelectingImage()
    }
    
    func requestCameraPermission() {
        
        AVCaptureDevice.requestAccess(for: .video, completionHandler: {accessGranted in
            guard accessGranted == true else {
                self.generateAlert(message: Constant.DisableCamera)
                return
            }
            self.openCamera()
        })
    }
    
    private func openCamera() {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let image = info[UIImagePickerControllerEditedImage] as! UIImage
        dismiss(animated: true, completion: nil)
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        activityIndicatorView.center = self.view.center
        activityIndicatorView.startAnimating()
        
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        
        if let error = error {
            generateAlert(message: error.localizedDescription)
        } else {
            activityIndicatorView.stopAnimating()
            isCameraTakingPic = true
            fetchAssets()
        }
    }
    
}


// MARK: CollectionView Delegate

extension SFRootPickerViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: ((self.view.frame.size.width - 40) / numberOfCellInRow), height: ((self.view.frame.size.width - 40) / numberOfCellInRow))
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return numberOfSectionKeys.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let rowkey = numberOfSectionKeys[section] as String
        let numberOfRow = totalAssetItems[rowkey] as! [PhotoCollection]
        if isCameraPreview && section == 0 {
            return numberOfRow.count + 1
        }
        return numberOfRow.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        var reusableView : SFHeaderCollectionReusableView? = nil
        
        if kind == UICollectionElementKindSectionHeader {
            
            reusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: SFHeaderCollectionReusableView.reuseId, for: indexPath) as? SFHeaderCollectionReusableView
            reusableView?.lblDate.text = (numberOfSectionKeys[indexPath.section] as String).uppercased()
        }
        
        return reusableView!
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if isCameraPreview && indexPath.section == 0 && indexPath.row == 0 {
            // Camera
            guard let imageCell = collectionView.dequeueReusableCell(withReuseIdentifier: SFImageCollectionViewCell.reuseId, for: indexPath) as? SFImageCollectionViewCell else {
                return UICollectionViewCell()
            }
            
            imageCell.cameraView = true
            return imageCell
        }
        
        let assets = findAsset(indexPath: indexPath)

        if assets.mediaType == .image {
            
            guard let imageCell = collectionView.dequeueReusableCell(withReuseIdentifier: SFImageCollectionViewCell.reuseId, for: indexPath) as? SFImageCollectionViewCell else {
                return UICollectionViewCell()
            }
            imageCell.selectedAssetItems = self.selectedAssetItems
            imageCell.photoAsset = assets
            return imageCell
        }
        
        if assets.mediaType == .video {
            
            guard let videoCell = collectionView.dequeueReusableCell(withReuseIdentifier: SFVideoCollectionViewCell.reuseId, for: indexPath) as? SFVideoCollectionViewCell else {
                return UICollectionViewCell()
            }
            videoCell.selectedAssetItems = self.selectedAssetItems
            videoCell.photoAsset = assets
            return videoCell
        }
        return UICollectionViewCell()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if isCameraPreview && indexPath.section == 0 && indexPath.row == 0 {
            
            requestCameraPermission()
            return
        }
       
        let assets = findAsset(indexPath: indexPath)
        
        if let selectedArray = selectedAssetItems {
            
            if selectedArray.contains(assets) {
                selectedAssetItems?.remove(at: selectedArray.index(of: assets)!)
            } else {
                selectedAssetItems?.append(assets)
            }
        }
        
        actionAfterSelectingImage()
        photoCollectionView.reloadItems(at: [indexPath])

        
    }
    
    private func actionAfterSelectingImage() {
        
        let totalSelectedItems: Int = (selectedAssetItems?.count)!
        self.titleNavigateView.title = "Selected \(totalSelectedItems)"
        let selectedItemCount = selectedAssetItems?.count ?? 0
        if isAutoDismissOnMaximum && maximumSelectionCount == selectedItemCount {
            
            self.doneButtonTapped(self)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        
        guard maximumSelectionCount > 0 else { return true }
        if isCameraPreview && indexPath.section == 0 && indexPath.row == 0 {
            return true
        }
        
        let assets = findAsset(indexPath: indexPath)
        
        if let selectedArray = selectedAssetItems {
            
            if selectedArray.contains(assets) {
                return true
            }
        }

        let selectedItemCount = selectedAssetItems?.count ?? 0
        if maximumSelectionCount <= selectedItemCount {
            
            self.generateAlert(message: "You cannot select more than \(maximumSelectionCount ?? 0) images.")
            return false
        }
        
        return true
    }
    
//    MARK: Collection View Helper
    private func findAsset(indexPath: IndexPath) -> PHAsset {
        
        let rowkey = numberOfSectionKeys[indexPath.section] as String
        let rowArray = totalAssetItems[rowkey] as! [PhotoCollection]
        let rowDict = rowArray[ isCameraPreview && indexPath.section == 0 ? (indexPath.row - 1) : indexPath.row]
        return rowDict.assets
    }
    
}
