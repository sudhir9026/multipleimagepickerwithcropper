//
//  SFHeaderCollectionReusableView.swift
//  multiImageSDK
//
//  Created by sudhir on 9/27/18.
//  Copyright © 2018 Sofmen. All rights reserved.
//

import UIKit

class SFHeaderCollectionReusableView: UICollectionReusableView {

    
    static let reuseId = Constant.HeaderCellIndentifier
    
    @IBOutlet weak var lblDate: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
