//
//  SFImageCollectionViewCell.swift
//  multiImageSDK
//
//  Created by sudhir on 9/25/18.
//  Copyright © 2018 Sofmen. All rights reserved.
//

import UIKit
import Photos
class SFImageCollectionViewCell: UICollectionViewCell {

    
    static let reuseId = Constant.ImageCellIndentifier

    @IBOutlet weak var listImageView: UIImageView!
    @IBOutlet weak var btnSelect: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var photoAsset: PHAsset? {
        didSet {
            self.btnSelect.isHidden = false
            loadPhotoAsset()
        }
    }
    
    var cameraView: Bool? {
        didSet {
            self.btnSelect.isHidden = true
            self.listImageView.image = UIImage.init(named: Constant.CameraJPG)
        }
    }
    var selectedAssetItems : [PHAsset]? = []
    
    private func loadPhotoAsset() {

        guard let asset = photoAsset else { return }
        
        self.listImageView.fetchImage(asset: asset, contentMode: .aspectFill, targetSize: self.frame.size)
        if (selectedAssetItems?.contains(asset))!{
            
            self.btnSelect.isSelected = true
            self.cellBorder(isTrue: true)
        } else {
            self.btnSelect.isSelected = false
            self.cellBorder(isTrue: false)

        }
    }
}

extension UICollectionViewCell {
    
    func cellBorder(isTrue: Bool) {
        self.layer.borderColor = UIColor.blue.cgColor
        self.layer.borderWidth = isTrue ? 2: 0
    }
}


extension UIImageView {
    
    func fetchImage(asset: PHAsset, contentMode: PHImageContentMode, targetSize: CGSize) {
        
        let options = PHImageRequestOptions()
        options.deliveryMode = .highQualityFormat
        options.version = .original
        
        PHImageManager.default().requestImage(for: asset, targetSize: targetSize, contentMode: contentMode, options: options) { image, _ in
            
            guard let image = image else { return }
            switch contentMode {
            case .aspectFill:
                self.contentMode = .scaleAspectFill
            case .aspectFit:
                self.contentMode = .scaleAspectFit
            }
            self.image = image
        }
    }
    
}
