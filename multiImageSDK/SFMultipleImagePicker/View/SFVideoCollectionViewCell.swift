//
//  SFVideoCollectionViewCell.swift
//  multiImageSDK
//
//  Created by sudhir on 9/25/18.
//  Copyright © 2018 Sofmen. All rights reserved.
//

import UIKit
import Photos

class SFVideoCollectionViewCell: UICollectionViewCell {

    
    static let reuseId = Constant.VideoCellIndentifier

    
    @IBOutlet weak var videoImageView: UIImageView!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var btnSelect: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var photoAsset: PHAsset? {
        didSet {
            loadPhotoAsset()
        }
    }
    
    var selectedAssetItems : [PHAsset]? = []
    
    private func loadPhotoAsset() {
        
        guard let asset = photoAsset else { return }
        
        self.videoImageView.fetchImage(asset: asset, contentMode: .aspectFill, targetSize: self.frame.size)
        self.lblDuration.text = seconds2Timestamp(intSeconds: asset.duration)
        if (selectedAssetItems?.contains(asset))!{
            
            self.btnSelect.isHidden = false
            self.cellBorder(isTrue: true)

        } else {
            self.btnSelect.isHidden = true
            self.cellBorder(isTrue: false)

        }
    }
    
    private func seconds2Timestamp(intSeconds: Double) -> String {
        
        let mins:Int = Int(round(((intSeconds / 60))))
        let secs:Int = Int(round(intSeconds.truncatingRemainder(dividingBy: 60)))
        
        let strTimestamp:String = ((mins<10) ? "0" : "") + String(mins) + ":" + ((secs<10) ? "0" : "") + String(secs)
        return strTimestamp
        
    }
}
