//
//  ViewController.swift
//  multiImageSDK
//
//  Created by sudhir on 9/25/18.
//  Copyright © 2018 Sofmen. All rights reserved.
//

import UIKit
import Photos

class ViewController: UIViewController {

    
    @IBOutlet var colletionViewHeight: NSLayoutConstraint!
    @IBOutlet var selectedImageCollectionView: UICollectionView!
    @IBOutlet var optionListTableView: UITableView!
    
    let optionList = ["Only Photo", "Only Video", "Both Photo and Video", "Photo with limit 5", "Photo with camera preview", "Auto dismiss when total selection complete"]
    
    private var selectedAssetItems : [PHAsset]? = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        colletionViewHeight.constant = 0
        
        // Do any additional setup after loading the view, typically from a nib.
        
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension ViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return optionList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        if cell == nil {
            cell = UITableViewCell.init(style: .default, reuseIdentifier: "Cell")
        }
        cell?.textLabel?.text = optionList[indexPath.row]
        return cell!
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        tableView.deselectRow(at: indexPath, animated: true)
        
        let imagePicker = SFImagePickerManager()
        
        switch indexPath.row {

        case 0:
            imagePicker.selectMediaType = .OnlyPhoto
        case 1:
            imagePicker.selectMediaType = .OnlyVideo
        case 2:
            imagePicker.selectMediaType = .Both
        case 3:
            imagePicker.maximumSelectionCount = 5
        case 4:
            imagePicker.cameraPreview = true
        case 5:
            imagePicker.maximumSelectionCount = 5
            imagePicker.isAutoDismissOnMaximum = true
            
        default:
            print("default")
        }
        
        imagePicker.presentSDImagePickerController(self, selectBlock: { (assets) in

            self.selectedAssetItems = assets
            self.selectedImageCollectionView.reloadData()
            self.colletionViewHeight.constant = 160

        }) {
            print("Canceled")
        }
        
    }
    
}

extension ViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return (selectedAssetItems?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectedCell", for: indexPath) as! SelectedCollectionViewCell
        
        let assets = selectedAssetItems![indexPath.row]
        cell.videoPlayImage.isHidden = true
        
        cell.photoImage.fetchImage(asset: assets, contentMode: .aspectFill, targetSize: cell.frame.size)
        if assets.mediaType == .video {
            cell.videoPlayImage.isHidden = false
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let asset = self.selectedAssetItems![indexPath.row]
        if asset.mediaType == .video {
            SFImagePickerManager().playVideo(self, asset: asset)
        }
    }
    
    
}

